Jan. 4 - 10, 2016
=================

#### MPU6050 and 9150 shenanigans

*   Used breakout board to test interfacing with MPU
*   Used Arduino to calibrate MPU6050

	*   Plans to port calibration to STM32F4

*   Attempts to run sensor fusion on STM32F4

	*   So far without success:
	*   Appears to have correct raw measurements
	*   Seems to be problem in fusion / AHRS calculation
	*   Will test using original AHRS code (not refactored version)
