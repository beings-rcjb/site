Jan. 23 - 29, 2017
==================

## Robot <small>(related stuff)</small>

### PIDs

* PID controller for motor speed:
  * Target encoder speed set as power level &times;&nbsp;scalar
  * PID fed error from target encoder speed
  * PID output is change in motor power (i.e. is accumulated)

## Development <small>(related stuff)</small>

### GitLab!

* Moved repo onto [GitLab](//gitlab.com/beings-rcjb/)
  * Issue tracker now being used instead of Trello

### CMake!

* Configured CMake build to remove dependence on Eclipse for build
  * Allows for [CI builds][gl-ci] run on GitLab

[gl-ci]: //gitlab.com/beings-rcjb/robot/pipelines

