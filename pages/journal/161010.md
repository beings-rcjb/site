Oct. 10 - 16, 2016
==================

### Ping sensor mounts

* Printed mount - screw holes are out of alignment and pin slot is too small
* Re-measure everything - pins are 0.1" pitch not 2mm! - find that transducers
  are probably also out of alignment
