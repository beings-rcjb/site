Nov. 18 - Dec. 10, 2017
=======================

NanoPi setup
------------

### Goals
- Use Docker to deploy code in snapshottable containers
- Use [resin.io](//resin.io) / [Resin Supervisor][resin-sup] to remotely
  manage/deploy from development machines
- Utilize a serial link for 2 way frame-based communication between the Pi and
  the STM32F4

### ResinOS/Yocto Adventures
- Found [resin-allwinner][resin-allwinner] - still incomplete/*supposedly* being
  worked on
  - Does not support AP66212/AP6212A WiFi/BT chipsets
  - I used [a build from @shaunmulligan][res-aw-build]
- Attempts to patch:
  - Could not get resin-allwinner (from fresh clone) to build on my Arch system
  - Copying driver blobs onto SD card did not seem to do anything
  - Possibly missing patch to root device tree overlay for enabling WiFi device
    but did not get to test

### Armbian mainline
- Armbian distro has patched mainline device trees to include WiFi/BT
- No patches for USB OTG
  - Attempt to create overlay adding in `usbphy`, OTG controller, and ID pins
    - Overlay fails to load with an invalid address (?), and causes boot script
      to also remove all other overlays
  - Patch base overlay from mainline (torvalds/linux):
    - Grab and apply patches from [Armbian tree][armbian-build], then OTG
      changes
    - Build (`DTC_FLAGS=-@ ARCH=arm`) on laptop and copy into device trees on
      the SD card
    - Caveat: must recopy/rebuild device tree for every kernel update

### Containers
- Docker installed without incident
- Resin supervisor:
  - Container will execute, have not spent time to create configuration / start
    script based on ResinOS/meta-resin for mounts/volumes/etc

[resin-sup]:       //github.com/resin-io/resin-supervisor
[resin-allwinner]: //github.com/shaunmulligan/resin-allwinner
[res-aw-build]:    //github.com/resin-os/resin-os-device-support/issues/38#issuecomment-320237321
[armbian-build]:   //github.com/armbian/build
