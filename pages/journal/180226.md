Feb. 26 - March 4, 2018<span class="text-muted fa fa-fw fa-picture-o"></span>
=======================

## Hardware

- Mirror wraps around back of cover to stay on more securely - slots cut out for
  various screws in the way
- Fixed slot positions on Pi mount
- Fixed size of holes in Pi mount top plate

![Mirror mount](/img/180226/mirror.png)
