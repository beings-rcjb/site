Feb. 27 - March 5, 2017
=======================

## Software

### Algorithm refactor into pseudo-singleton object ([#17](//gitlab.com/beings-rcjb/robot/issues/17))

* Algorithm made into object keeping state about what movements (current cell, moving to, etc.) are happening
  * Move functions use this state to make decisions on whether or not to detect for victims
    * Allows for victim detection to work on turns (for victims in corners)

### Confirmation for clearing maze data ([#4](//gitlab.com/beings-rcjb/robot/issues/4))

### Fixed calls to align after dropping victim ([#16](//gitlab.com/beings-rcjb/robot/issues/16))
