Jan. 30 - Feb. 5, 2017
======================

## Software

### Movement

* Implemented API to pass custom move info objects to move code
  * objects instruct on additional corrections and when to stop
  * goal is to implement ramp using this API and unify all movement
