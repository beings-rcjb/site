Mar. 28 - Apr. 3, 2016
======================

### Software

#### Ramp

*   Implemented ramp algorithm (see commit `1960ba6`)
*   Traveling ramp is not aligned, and may collide with walls
*   Need some sort of active alignment while traversing ramp
*   Top / bottom of just-discovered tile is not properly marked as visited

#### Alignment improvements

*   Wait for fresh data before aligning
*   Correct distance from front wall if there is one in front
*   Aligns called before every movement function
*   See commits `fa4721c` and `9c7abfe`

#### Black tile

*   Implemented black tile and solved merge conflicts
*   Bugfixes all around:

*   Previously was marking the wrong tile as black
*   Ignoring black tiles in pathing didn't always work

### Hardware

#### Battery terminal reinforcement

*   Reinforced with shrink-wrap to prevent terminals from being pulled out of socket

#### LED Light

*   Bright round white LED created for victim signalling
*   Still needs to be soldered to robot
*   Uses 105Ω current-limiting resistor
