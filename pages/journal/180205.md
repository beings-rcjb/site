Feb. 5 - 11, 2018<span class="text-muted fa fa-fw fa-picture-o"></span>
=================

## Hardware

Created mounts for:

- NanoPi:
  - Designed to be slotted in like "rack-mount"
  - Sandwiches NanoPi board between boards with wings and protrusions to mount
    in slots
- Frame front support:
  - Supports frame at front between it and chassis deck - prevents sagging
  - Has slots for placing NanoPi mount
- Webcam:
  - Tray style mount for webcam
- Mirror:
  - Mounted to top cover plate via dimple and screw hole

![Mirror with NanoPi mount](/img/180205/all.png)
![Mirror mount](/img/180205/mirror.png)
![Front support](/img/180205/support1.png)
![Camera mount top](/img/180205/cammount_top.png)
![Camera mount bottom](/img/180205/cammount_bot.png)
![NanoPi mount full](/img/180205/npi_full.png)
![NanoPi mount halves](/img/180205/npi_halves.png)
