var _ = require('lodash');
var yargs = require('yargs');
var gulp = require('gulp');
var gutil = require('gulp-util');
var plumber = require('gulp-plumber');
var async = require('async');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var markdown = require('gulp-markdown');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var cleanCss = require('gulp-clean-css');
var pug = require('gulp-pug');
var htmlmin = require('gulp-htmlmin');
var glob = require('glob');
var fs = require('fs');
var path = require('path');
var cheerio = require('cheerio');
var reload = require('gulp-livereload');
var child = require('child_process');

var argv = yargs
  .option('d', {
    alias: 'dev',
    describe: 'Build in develop mode',
    type: 'boolean'
  })
  .option('r', {
    alias: 'release',
    describe: 'Build in release mode',
    type: 'boolean'
  })
  .option('p', {
    alias: 'port',
    describe: 'Port for livereload server (in dev mode)',
    default: 35729,
    type: 'number'
  })
  .argv;

var is_dev = true;
{
  // if (process.env.NODE_ENV) {
  //   if (process.env.NODE_ENV == 'production') {
  //     is_dev = false;
  //   } else if (process.env.NODE_ENV == 'development') {
  //     is_dev = true;
  //   } else {
  //     if (argv.r) {
  //       is_dev = false;
  //       process.env.NODE_ENV = 'production';
  //     }
  //     if (argv.d) {
  //       is_dev = true;
  //       process.env.NODE_ENV = 'development';
  //     }
  //   }
  // } else {
    if (argv.r) {
      is_dev = false;
      // process.env.NODE_ENV = 'production';
    }
    if (argv.d) {
      is_dev = true;
      // process.env.NODE_ENV = 'development';
    }
  // }
  if (is_dev) {
    gutil.log('Building for ' + gutil.colors.cyan('DEVELOPMENT'));
  } else {
    gutil.log('Building for ' + gutil.colors.cyan('RELEASE'));
  }
}

gulp.task('bower', () => {
  var bower = require('main-bower-files');
  var bowerNormalizer = require('gulp-bower-normalize');
  return gulp.src(bower(), {base: './bower_components'})
    .pipe(plumber())
    .pipe(bowerNormalizer())
    .pipe(gulp.dest('dist/bower_assets'));
})


var paths = {
  markdown: {
    src:  'pages/journal/*.md',
    dest: 'pages/journal/'
  },
  journal: {
    src:  'pages/journal/journal.pug',
    dest: 'dist/',
    pages: 'pages/journal/*[0-9].html'
  },
  pug: {
    src: ['pages/*/*.pug', '!pages/journal/journal.pug'],
    dest: 'dist/'
  },
  index: {
    src: ['dist/**/*.html', '!dist/**/index.html'],
    dest: 'dist/'
  },
  sass: {
    src:  'css/*.s[ac]ss',
    dest: 'css/'
  },
  css: {
    src:  'css/*.css',
    dest: 'dist/',
    outFile: 'styles.css',
    maps: 'maps/'
  },
  js: {
    src: 'js/**/*',
    dest: 'dist/',
    outFile: ''
  },
  publicFiles: {
    src: 'public/**/*',
    dest: 'dist/'
  }
};

gulp.task('markdown', () => {
  var renderer = new markdown.marked.Renderer();
  renderer.heading = (text, level) => {
    return '<h' + level + '>' + text + '</h' + level + '>\n';
  };
  renderer.image = (href, title, text) => {
    var out = '<img class="img" src="' + href + '" alt="' + text + '"';
    if (title) {
      out += ' title="' + title + '"';
    }
    out += renderer.options.xhtml ? '/>' : '>';
    return out;
  }
  return gulp.src(paths.markdown.src)
    .pipe(plumber())
    .pipe(markdown({renderer: renderer}))
    .pipe(gulp.dest(paths.markdown.dest))
    .pipe(reload());
});

gulp.task('journal', ['markdown'], (cb) => {
  var pages_fns = glob.sync(paths.journal.pages);
  var pages = [];
  async.each(pages_fns, (page_fn, cb) => {
    fs.readFile(page_fn, 'utf8', (err, data) => {
      if (!err) {
        var page = {heading: '', id: '', content: ''};
        var $ = cheerio.load(data);
        page.id = path.basename(page_fn, path.extname(page_fn));
        page.heading = $('h1').html();
        $('h1').remove();
        page.content = $.html();
        pages = _.concat(pages, page);
      } else {
        gutil.log(gutil.colors.cyan('Journal'), gutil.colors.red('file read error:'));
        gutil.log(err.toString());
      }
      cb();
    });
  }, () => {
    pages = _.orderBy(pages, ['id'], ['asc']);
    gulp.src(paths.journal.src)
      // .pipe(plumber())
      .pipe(pug({
        locals: {entries: pages, ENV_DEVELOPMENT: is_dev, LR_PORT: argv.p}
      }))
      .pipe(htmlmin({collapseWhitespace: true}))
      .pipe(gulp.dest(paths.journal.dest))
      .pipe(reload());
    cb();
  });
});

gulp.task('pug', () => {
  return gulp.src(paths.pug.src)
    // .pipe(plumber())
    .pipe(pug({
      locals: {ENV_DEVELOPMENT: is_dev, LR_PORT: argv.p}
    }))
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(rename({
      dirname: ''
    }))
    .pipe(gulp.dest(paths.pug.dest))
    .pipe(reload());
})

gulp.task('templates', ['pug', 'journal']);

gulp.task('sass', () => {
  return gulp.src(paths.sass.src)
    .pipe(plumber())
    .pipe(sourcemaps.init())
      .pipe(sass.sync({
        outputStyle: "expanded"
      }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(paths.sass.dest))
    .pipe(reload());
})

gulp.task('css', ['sass'], () => {
  return gulp.src(paths.css.src)
    .pipe(plumber())
    .pipe(sourcemaps.init({loadMaps: true}))
      .pipe(concat(paths.css.outFile))
      .pipe(autoprefixer())
      .pipe(cleanCss())
    .pipe(sourcemaps.write(paths.css.maps))
    .pipe(gulp.dest(paths.css.dest))
    .pipe(reload());
});

gulp.task('index', () => {
  return gulp.src(paths.index.src)
    .pipe(rename((path) => {
      path.dirname += '/' + path.basename;
      path.basename = 'index';
    }))
    .pipe(gulp.dest(paths.index.dest))
    .pipe(reload());
})

gulp.task('html', ['templates', 'index']);

gulp.task('js', () => {
  //return gulp.src(paths.js.src, {base: paths.js.src})
  return gulp.src(paths.js.src)
    .pipe(gulp.dest(paths.js.dest))
    .pipe(reload());
})

gulp.task('public', () => {
  //return gulp.src(paths.publicFiles.src, {base: paths.publicFiles.src})
  return gulp.src(paths.publicFiles.src)
    .pipe(gulp.dest(paths.publicFiles.dest))
    .pipe(reload());
})

gulp.task('build', ['bower', 'css', 'html', 'js', 'public']);

gulp.task('watch', ['build'], () => {
  gulp.watch(paths.markdown.src, ['markdown', 'journal']);
  gulp.watch(paths.journal.src, ['journal']);
  gulp.watch(paths.pug.src, ['pug']);
  gulp.watch(paths.index.src, ['index']);
  gulp.watch(paths.css.src, ['css']);
  gulp.watch(paths.sass.src, ['sass']);
  gulp.watch(paths.js.src, ['js']);
  gulp.watch(paths.publicFiles.src, ['public']);
});

gulp.task('serve', ['watch'], () => {
  reload.listen();
  child.fork('./bin/www', process.argv);
})

gulp.task('default', ['build']);
