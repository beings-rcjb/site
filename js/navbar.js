$(document).ready(function() {
  $('.navbar button.navbar-toggler').click(function() {
    var content = $('#content');
    if (content.hasClass('navbar-open')) { // Close it
      content.removeClass('navbar-open');
      content.off('click');
      $('.navbar-nav-container').off('mousedown click');
    } else {
      content.addClass('navbar-open');
      content.click(function() {
        $(this).click(function() { // Hack to ignore click that attached handler
          $(this).removeClass('navbar-open')
          $(this).off('click');
        })
      });
    }
  })
});
