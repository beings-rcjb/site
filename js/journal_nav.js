$(document).ready(function() {
	img_attach_popups("img");

	$(".journal_link").click(function() {
		var frame = $("#" + $(this).attr("id") + "_data");
		var nav_icon = $("#" + $(this).attr("id") + "_navico");
		if (frame.hasClass("collapse")) {
			if (frame.hasClass("in")) {
				frame.collapse("hide");
				nav_icon.removeClass('nav_expanded');
				nav_icon.addClass('nav_collapsed');
			} else {
				frame.collapse("show");
				nav_icon.removeClass('nav_collapsed');
				nav_icon.addClass('nav_expanded');
			}
		}
	});
});
