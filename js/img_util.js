
function img_make_links() {
	$("img.link").wrap(function() {
		return "<a href=\"" + $(this).attr("src") + "\"></a>";
	});
}

function img_destroy_popup(cont_div) {
	cont_div.animate({opacity: '0'},	250, "linear", function() {
		cont_div.remove();
	});
}

function img_attach_popups(t_class) {
	$("img." + t_class).click(function () {
		var img = $(this);
		$("body").append(function () {
			return img.clone().addClass("_popup_img link fill_parent");
		});
		$("._popup_img").wrap("<div class=\"blackout centered_container _popup_img_div\"></div>");
		$("._popup_img_div").css("opacity", "0").animate({opacity: '1'}, 250, "linear");
		$("._popup_img_div").click(function () {
			$(this).animate({opacity: '0'}, 250, "linear", function () {
				$(this).remove();
			});
		});
	});
	$(".popup_block").click(function () {
		$(this).remove();
	});
}

$(window).load(function() {
	img_make_links();
	img_attach_popups("popup");
});

$(document).keypress(function(event) {
	if (event.keyCode == 27) {
		img_destroy_popup($("._popup_img_div"));
	}
});
