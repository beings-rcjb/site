var express = require('express');
var compression = require('compression');

var argv = require('yargs')
  .option('l', {
    alias: 'log',
    describe: 'Enable logging',
    type: 'boolean'
  })
	.argv;

var app = express();

if (argv.l) {
	app.use(require('morgan')('dev'));
}

app.use(compression());

app.use(express.static('dist', {'dotfiles': 'ignore'}));

module.exports = app;
